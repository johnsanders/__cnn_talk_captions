import React from "react";
import LiNavLink from "./LiNavLink";
import PropTypes from "prop-types";

const NavItem = (props) => (
	<LiNavLink exact activeClassName="active" to={props.link}>
		<span className={props.iconClasses}></span>
		&nbsp;
		<span className={props.textClasses}>
			{props.text}
		</span>
	</LiNavLink>
);

NavItem.propTypes = {
	link:PropTypes.string.isRequired,
	iconClasses:PropTypes.string,
	textClasses:PropTypes.string,
	text:PropTypes.string
};

export default NavItem;
