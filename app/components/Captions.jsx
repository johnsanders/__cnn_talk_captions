import React from "react";
import PropTypes from "prop-types";
import SocketIo from "socket.io-client";
import autobind from "react-autobind";

class Captions extends React.Component {
	constructor(props){
		super(props);
		autobind(this);
		this.positions = {top:"-50px", middle:"-7px", bottom:"32px", bottomStart:"80px"};
		this.state = {
			line0:"",
			line1:"",
			line2:"",
			line0Pos:this.positions.bottomStart,
			line1Pos:this.positions.middle,
			line2Pos:this.positions.bottom
		}; 
		this.activeLine = 2;
		this.lineLength = 40;
	}
	componentDidMount(){
		this.socketIo = new SocketIo("http://cnnitouch:3000");
		this.socketIo.emit("join", "captions");
		this.socketIo.on("text", this.onText);
	}
	onText(text){
		if ( text.indexOf(">") != -1 && !this.justScrolled) {
			this.handleNewSpeaker();
		} else if ( text.indexOf("\r\n") !== -1  ) {
			text = this.handleNewline(text);
		} else {
			this.justScrolled = false;
		}
		const lineKey = "line" + this.activeLine.toString(); 
		this.setState({[lineKey]:this.state[lineKey] + text});
	}
	scroll(activeLine, prevLine, nextLine, positions){
		this.resetTimeout();
		this.setState({
			["line" + activeLine.toString() + "Pos"]:positions.middle,
			["line" + nextLine.toString() + "Pos"]:positions.bottom,
			["line" + prevLine.toString() + "Pos"]:positions.top,
		});
		setTimeout( () => {
			this.setState( {
				["line" + prevLine.toString() + "Pos"]:positions.bottomStart,
				["line" + prevLine.toString()]:""
			});
		}, 50 );
		this.justScrolled = true;
	}
	handleNewline(text){
		const lineKey = "line" + this.activeLine.toString(); 
		if (  this.state[lineKey].length < this.lineLength ) {
			text = text.replace("\r\n", " ");
		} else {
			const split = text.split("\r\n");
			this.scroll(this.activeLine, this.getPrevLineNum(this.activeLine), this.getNextLineNum(this.activeLine), this.positions);
			this.setState({
				[lineKey]: this.state[lineKey] + split[0]}
			);
			this.activeLine = this.getNextLineNum(this.activeLine);
			text = split[1];
		}
		return text;
	}
	handleNewSpeaker(){
		this.scroll(this.activeLine, this.getPrevLineNum(this.activeLine), this.getNextLineNum(this.activeLine), this.positions);
		this.activeLine = this.getNextLineNum(this.activeLine);
	}
	resetTimeout(){
		clearTimeout(this.timeout);
		this.timeout = setTimeout( this.clearCaptions, 6000 );
	}
	clearCaptions(){
		this.scroll(this.activeLine, this.getPrevLineNum(this.activeLine), this.getNextLineNum(this.activeLine), this.positions);
		this.activeLine = this.getNextLineNum(this.activeLine);
		setTimeout( () => {
			this.scroll(this.activeLine, this.getPrevLineNum(this.activeLine), this.getNextLineNum(this.activeLine), this.positions);
			this.activeLine = this.getNextLineNum(this.activeLine);
		}, 1000 );
	}
	getPrevLineNum(lineNum){
		return lineNum === 0 ? 2 : lineNum - 1;
	}
	getNextLineNum(lineNum){
		return lineNum === 2 ? 0 : lineNum + 1;
	}
	render(){
		return (
			<div className="captions">
				<div 
					style={{
						transform:"translate(0px," + this.state.line0Pos + ")",
					}}
					dangerouslySetInnerHTML={{__html:this.state.line0}} 
				/>
				<div 
					style={{
						transform:"translate(0px," + this.state.line1Pos + ")",
					}}
					dangerouslySetInnerHTML={{__html:this.state.line1}} 
				/>
				<div 
					style={{
						transform:"translate(0px," + this.state.line2Pos + ")",
					}}
					dangerouslySetInnerHTML={{__html:this.state.line2}} 
				/>
			</div>
		);
	}
}

Captions.propTypes = {
};

export default Captions;
