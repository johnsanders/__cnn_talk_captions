import React from "react";
import {render} from "react-dom";
import "./styles/style.css";
import Captions from "./components/Captions";

render( <Captions />, document.getElementById("app"));
