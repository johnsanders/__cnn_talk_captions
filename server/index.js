const net = require("net");
const http = require("http");
const SocketIo = require("socket.io");
const ccHost = "10.191.38.44";
const ccPort = 2400;

const socketIo = new SocketIo();
let ccConnected = false;
let ccSocket;

const app = http.createServer( (req, res) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.end();
});

socketIo.on('connection', client => {
	client.on('join', room => {
		client.join(room);
		if (room === "admin") {
			socketIo.to(client.id).emit('ccConnected', ccConnected);
			client.on('ccAction', command => {
				if (command === "connect") {
					connect();
				} else if (command === "disconnect") {
					disconnect();
				}
			});
		}
	});
	console.log(client.id + " joined.");
});

const connect = () => {
	ccSocket = net.createConnection(ccPort, ccHost);
	ccSocket.on('connect', () => {
		ccConnected = true;
		socketIo.to('admin').emit('ccConnected', ccConnected);
	});
	ccSocket.on('data', data => {
		socketIo.to('captions').emit('text', data.toString());
	});
	ccSocket.on('end', () => {
		ccConnected = false;
		socketIo.to('admin').emit('ccConnected', ccConnected);
	});
};

const disconnect = () => {
	ccSocket.end( () => {
		setTimeout( () => {
			ccSocket.destroy();
		}, 1000 );
	});
};

app.listen(3000);
socketIo.listen(app);
